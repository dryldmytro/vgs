package com.dryl;

import com.dryl.model.RedactedCard;
import com.google.gson.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

public class VGS {
    private RedactedCard redactedCard;
    private String content;

    public VGS() {
        redactedCard = new RedactedCard();
    }

    public void sendDataInVGS(HttpEntity data, String routeType, boolean originalData) throws IOException, JAXBException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("https://tnt8ndroedj.sandbox.verygoodproxy.com/post");

        httpPost.setEntity(data);
        httpPost.setHeader("Content-Type", routeType);
        CloseableHttpResponse response = client.execute(httpPost);
        content = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);
        System.out.println("response=" + content);
        if (originalData) {
            redactedCard = createRedactedCard(content, routeType);
        }
        client.close();
    }

    public RedactedCard createRedactedCard(String content, String routeType) throws JAXBException {
        JsonElement element = new JsonParser().parse(content);
        JsonObject jsonResponse = element.getAsJsonObject();
        switch (routeType) {
            case "application/json":
                JsonObject jsonObject = jsonResponse.get("json").getAsJsonObject();
                redactedCard.setClientName(jsonObject.get("clientName").getAsString());
                redactedCard.setRedacted_cvv(jsonObject.get("cvv").getAsString());
                redactedCard.setRedacted_number(jsonObject.get("account_number").getAsString());
                return redactedCard;
            case "application/xml":
                String xmlObject = jsonResponse.get("data").getAsString()
                        .replaceAll("creditCard", "redactedCard")
                        .replaceAll("account_number", "redacted_number")
                        .replaceAll("cvv", "redacted_cvv");
                JAXBContext jaxbContext = JAXBContext.newInstance(RedactedCard.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                redactedCard = (RedactedCard) unmarshaller.unmarshal(new StringReader(xmlObject));
                return redactedCard;
            case "application/x-www-form-urlencoded":
                JsonObject formObject = jsonResponse.get("form").getAsJsonObject();
                redactedCard.setClientName(formObject.get("client_name").getAsString());
                redactedCard.setRedacted_cvv(formObject.get("cvv").getAsString());
                redactedCard.setRedacted_number(formObject.get("account_number").getAsString());
                return redactedCard;
        }
        return redactedCard;
    }

    public RedactedCard getRedactedCard() {
        return redactedCard;
    }

}
