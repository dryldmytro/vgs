package com.dryl;

import com.dryl.view.View;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, JAXBException {
        View view = new View();
        view.getUserData();
    }
}
