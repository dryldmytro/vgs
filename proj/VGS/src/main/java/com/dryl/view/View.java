package com.dryl.view;

import com.dryl.VGS;
import com.dryl.model.CreditCard;
import com.dryl.model.RedactedCard;
import com.google.gson.Gson;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class View {
    private static final Scanner scanner = new Scanner(System.in);
    private CreditCard creditCard;
    private VGS vgs;
    private Gson gson;

    public View() {
        creditCard = new CreditCard();
        vgs = new VGS();
        gson = new Gson();
    }

    public void getUserData() throws IOException, JAXBException {
        System.out.println("Hello. This is vgs test.\n Please, enter your name:\n");
        creditCard.setClientName(scanner.nextLine());
        System.out.println("Enter your account number:\n");
        creditCard.setAccount_number(scanner.nextLine());
        System.out.println("Enter your cvv:\n");
        creditCard.setCvv(scanner.nextLine());
        getUserRoute();
    }

    public void getUserRoute() throws IOException, JAXBException {
        System.out.println("Choose one of this route: json, xml, form:\n");
        String userChoice = scanner.nextLine().toLowerCase();
        switch (userChoice) {
            case "json":
                String json = gson.toJson(creditCard);
                vgs.sendDataInVGS(new StringEntity(json), "application/json", true);
                break;
            case "xml":
                String xml = xmlParser(creditCard, CreditCard.class);
                vgs.sendDataInVGS(new StringEntity(xml), "application/xml", true);
                break;
            case "form":
                List<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("client_name", creditCard.getClientName()));
                params.add(new BasicNameValuePair("account_number", creditCard.getAccount_number()));
                params.add(new BasicNameValuePair("cvv", creditCard.getCvv()));
                vgs.sendDataInVGS(new UrlEncodedFormEntity(params, "UTF-8"), "application/x-www-form-urlencoded", true);
                break;
            default:
                System.out.println("Try again. \n");
                getUserRoute();
        }
        getUserRevealChoice();
    }

    public void getUserRevealChoice() throws IOException, JAXBException {
        System.out.println("Would you like to reveal your data? (Enter: yes/no).\n");
        String userNumber = scanner.nextLine();
        switch (userNumber) {
            case "yes":
                RedactedCard redactedCard = vgs.getRedactedCard();
                System.out.println("Choose one of this route: json, xml, form:\n");
                String userChoice = scanner.nextLine().toLowerCase();
                switch (userChoice) {
                    case "json":
                        String json = gson.toJson(redactedCard);
                        vgs.sendDataInVGS(new StringEntity(json), "application/json", false);
                        break;
                    case "xml":
                        String xml = xmlParser(redactedCard, RedactedCard.class);
                        vgs.sendDataInVGS(new StringEntity(xml), "application/xml", false);
                        break;
                    case "form":
                        List<NameValuePair> params = new ArrayList<>();
                        params.add(new BasicNameValuePair("client_name", redactedCard.getClientName()));
                        params.add(new BasicNameValuePair("redacted_number", redactedCard.getRedacted_number()));
                        params.add(new BasicNameValuePair("redacted_cvv", redactedCard.getRedacted_cvv()));
                        vgs.sendDataInVGS(new UrlEncodedFormEntity(params, "UTF-8"), "application/x-www-form-urlencoded", false);
                        break;
                }
                break;
            case "no":
                System.out.println("Bye!");
                System.exit(0);
            default:
                System.out.println("Try again!");
                getUserRevealChoice();
        }
    }

    public String xmlParser(Object element, Class xmlClass) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(xmlClass);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(element, stringWriter);
        return stringWriter.toString();
    }
}
