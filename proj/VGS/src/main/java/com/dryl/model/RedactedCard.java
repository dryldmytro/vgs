package com.dryl.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RedactedCard {
    private String clientName;
    private String redacted_number;
    private String redacted_cvv;

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getRedacted_number() {
        return redacted_number;
    }

    public void setRedacted_number(String redacted_number) {
        this.redacted_number = redacted_number;
    }

    public String getRedacted_cvv() {
        return redacted_cvv;
    }

    public void setRedacted_cvv(String redacted_cvv) {
        this.redacted_cvv = redacted_cvv;
    }

    @Override
    public String toString() {
        return "RedactedCard{" +
                "clientName='" + clientName + '\'' +
                ", redacted_number='" + redacted_number + '\'' +
                ", redacted_cvv='" + redacted_cvv + '\'' +
                '}';
    }
}
