package com.dryl.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CreditCard {
    private String account_number;
    private String cvv;
    private String clientName;

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "accountNumber='" + account_number + '\'' +
                ", cvv='" + cvv + '\'' +
                ", clientName='" + clientName + '\'' +
                '}';
    }
}
